#include "iss.h"
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

uint16_t mem[(1<<16)-1];
uint16_t regs[4];

void load(char *buffer)
{
  char *token = strtok(buffer, "\n ");
  token = strtok(NULL, "\n ");
  token = strtok(NULL, "\n ");
  uint16_t addr = 0;
  while (token != NULL) {
    printf("token: %s\n", token);
    int n;
    int res = sscanf(token, "%x", &n);
    if (res != 1) {
      fprintf(stderr, "Error parsing input file.\n");
      exit(1);
    }
    mem[addr++] = (uint16_t)n;
    token = strtok(NULL, "\n ");
  }
  printf("\n");
}

int cpu(char *buffer)
{
  load(buffer);
  while (1)
  {
    // TODO
  }
  return 0;
}
